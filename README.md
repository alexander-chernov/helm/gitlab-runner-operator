# gitlab-runner-operator

![Version: 1.5.0](https://img.shields.io/badge/Version-1.5.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: v1.3.0](https://img.shields.io/badge/AppVersion-v1.3.0-informational?style=flat-square)

A helm chart for Gitlab Runner Operator, a declarative, GitOps based way of deploying your gitlab runners on kubernetes cluster

## What is this operator for
This operator permits you to run multiple runners with their own, unique configuration written in yaml (no more Toml, yay), following IAC approach

## Why
The official gitlab way consist in 2 steps registration, first you register your runner, and then you append certain features to it.

This breaks the IAC way, and doesn't permit you to be flexible in your configuration.

This operator aims to provide you with all configuration which are provided by [kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html), see below for some examples.

## Status & Contributions
This operator is still in alpha stages, so there is a possibility of breaking changes (which will be announced if any). Please open issues if you find any bugs

### Examples
In order to run your runner you will need to obtain a registration token ([see the official documentation](https://docs.gitlab.com/runner/register/))

#### Minimum configuration

In this config you only need to specify the name of your runner (`runner-sample`), registration token (`xxx`), and it's tag (`test-gitlab-runner`)
```yaml
apiVersion: gitlab.k8s.alekc.dev/v1beta1
kind: Runner
metadata:
  name: runner-sample
spec:
  environment:
    - "bar=foo"
  registration_config:
    token: "xxx"
    tag_list:
      - test-gitlab-runner
```

#### Using registration token inside a secret
If you prefer not to expose your registration token in the crd, you can specify the secret name.
Note that the token **MUST** be contained in the key `token`

```yaml
apiVersion: v1
data:
  token: c2VjcmV0LXRva2Vu
kind: Secret
metadata:
  name: gitlab-runner-token
type: Opaque
---
apiVersion: gitlab.k8s.alekc.dev/v1beta1
kind: Runner
metadata:
  name: runner-sample
spec:
  registration_config:
    token: "gitlab-runner-token"
    tag_list:
      - test-gitlab-runner
```

#### Mounting secrets or config maps as volumes
If you need to mount some config maps or secrets as volumes in your runner pods, you can easily achieve it with following config
```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: test-secret
stringData:
  foo: "this is foo"
  bar: "this is bar"
---
apiVersion: v1
kind: Secret
metadata:
  name: test-secret-2
stringData:
  zar: "this is zar"
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: test-config
data:
  foo.txt: |
    contents of foo
  bar.txt: |
    zzz
---
apiVersion: gitlab.k8s.alekc.dev/v1beta1
kind: Runner
metadata:
  name: runner-sample
spec:
  log_level: debug
  executor_config:
    image: "debian:slim"
    memory_limit: "150Mi"
    memory_request: "150Mi"
    volumes:
      config_map:
        - mount_path: /cm/
          name: test-config
      secret:
        - mount_path: /secrets/1/
          name: test-secret
        - mount_path: /secrets/2/
          name: test-secret-2
  registration_config:
    token: "xxx"
    tag_list:
      - test-gitlab-runner
```

## Installing the chart
To install the chart with the release name `my-release`
```
$ helm repo add alekc-charts https://charts.alekc.dev
$ helm install --name my-release alekc-charts/gitlab-runner-operator
```

---

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.jetstack.io | cert-manager | v1.7.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| cert-manager.install | bool | `false` | if true, will install cert-manager subchart. Use it only if you've set webhook.certType to `cert-manager` |
| cert-manager.installCRDs | bool | `true` |  |
| fullnameOverride | string | `"gitlab-runner-operator"` | full name override. It's set to gitlab-runner-operator since there is no sense in setting multiple instances of the operator |
| image.pullPolicy | string | `"IfNotPresent"` | pull policy to use for the operator image |
| image.repository | string | `"ghcr.io/alekc/gitlab-runner-operator"` | source repository |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| imagePullSecrets | list | `[]` | Image pull secrets |
| kubeTargetVersionOverride | string | `""` | Override detected kube version. Useful for rendering |
| nameOverride | string | `""` | override default name |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext.runAsNonRoot | bool | `true` |  |
| replicaCount | int | `1` |  |
| resources.limits.cpu | string | `"100m"` |  |
| resources.limits.memory | string | `"30Mi"` |  |
| resources.requests.cpu | string | `"100m"` |  |
| resources.requests.memory | string | `"20Mi"` |  |
| runners | list | `[]` | if set, will install default runners directly with the helm chart  - name: runner01    spec:      gitlab_instance_url: https://gitlab.com/      concurrent: 1      log_level: debug      registration_config:        token: "xxxx-EC37Dx7H4_5"        tag_list:          - test-gitlab-runner |
| securityContext.allowPrivilegeEscalation | bool | `false` |  |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.name | string | `"gitlab-runner-operator-controller-manager"` |  |
| tolerations | list | `[]` |  |
| webhook.certType | string | `"self-signed"` | webhooks require trusted tls certificates. You can provide it by either using cert-manager or self-signed |
| webhook.enabled | bool | `true` | Enable mutating and validating webhook. Note: for now gitlab-runner-operator will attempt to declare the hook regardless of this setting |
| webhook.selfSigned.affinity | object | `{}` |  |
| webhook.selfSigned.image | object | `{"pullPolicy":"IfNotPresent","repository":"k8s.gcr.io/ingress-nginx/kube-webhook-certgen","tag":"v1.1.1"}` | webhook image |
| webhook.selfSigned.image.repository | string | `"k8s.gcr.io/ingress-nginx/kube-webhook-certgen"` | which cert injector should be used |
| webhook.selfSigned.image.tag | string | `"v1.1.1"` | cert injector version |
| webhook.selfSigned.logLevel | string | `"debug"` | Set the logging level for patch operation |
| webhook.selfSigned.nodeSelector | object | `{}` |  |
| webhook.selfSigned.resources | object | `{"requests":{"cpu":"0.3","memory":"64Mi"}}` | resources for the injector |
| webhook.selfSigned.tolerations | list | `[]` |  |
